import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppsModule } from './apps/apps.module';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { LOCALE_ID, NgModule } from '@angular/core';

import { registerLocaleData } from '@angular/common';
import localeTh from '@angular/common/locales/th';
import { MomentDateFormatter } from './services/date-format.service';
import { ThaiDatePipe } from './pipes/thai-date.pipe';
import { LoginModule } from './login/login.module';
import { environment } from 'src/environments/environment';

registerLocaleData(localeTh, 'th');

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    AppRoutingModule,
    AppsModule,
    LoginModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'th' },
    { provide: NgbDateParserFormatter, useClass: MomentDateFormatter },
    { provide: 'API_URL', useValue: environment.apiUrl },
    { provide: 'SSJ_URL', useValue: environment.ssjUrl }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
