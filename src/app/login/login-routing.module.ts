import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginSSJComponent } from './login-ssj/login-ssj.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'login-ssj', component: LoginSSJComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
