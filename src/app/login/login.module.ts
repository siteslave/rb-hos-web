import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginSSJComponent } from './login-ssj/login-ssj.component';


@NgModule({
  declarations: [LoginComponent, LoginSSJComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    RouterModule
  ]
})
export class LoginModule { }
