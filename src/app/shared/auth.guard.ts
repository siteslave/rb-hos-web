import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  jwtHelper = new JwtHelperService();


  constructor(private router: Router) { }

  canActivate() {

    const token = sessionStorage.getItem('token');

    if (token) {
      const isExpired = this.jwtHelper.isTokenExpired(token);
      if (isExpired) {
        this.router.navigateByUrl('/login');
        return false;
      } else {
        return true;
      }

    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }

}
