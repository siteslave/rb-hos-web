import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThaiDatePipe } from './pipes/thai-date.pipe';
import { ShortTimePipe } from './pipes/short-time.pipe';



@NgModule({
  declarations: [ThaiDatePipe, ShortTimePipe],
  imports: [
    CommonModule
  ],
  exports: [ThaiDatePipe, ShortTimePipe]
})
export class SharedModule { }
