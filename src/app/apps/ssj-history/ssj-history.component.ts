import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-ssj-history',
  templateUrl: './ssj-history.component.html',
  styles: []
})
export class SsjHistoryComponent implements OnInit {

  items: any = [];
  loading = false;

  total = 0;
  offset = 0;
  page = 1;
  pageSizeItems: any = [20, 30, 40, 50, 100];
  pageSize = 20;

  query: any = '';

  constructor(private apiService: ApiService, private alertService: AlertService) { }

  ngOnInit() {
    this.getHistory();
  }

  onPageChange(event: number) {
    const _currentPage = +event;
    let _offset = 0;
    if (_currentPage > 1) {
      _offset = (_currentPage - 1) * this.pageSize;
    }
    this.offset = _offset;
    this.getHistory();
  }

  doSearch(event: any) {
    if (event.keyCode === 13) {
      this.getHistory();
    }
  }

  async getHistory() {
    try {
      this.loading = true;
      const rs: any = await this.apiService.getSSJHistory(this.query, this.pageSize, this.offset);
      this.loading = false;
      if (rs.ok) {
        this.items = rs.rows;
        this.total = rs.total;
      } else {
        this.alertService.alert(rs.error, 'error');
      }
    } catch (error) {
      this.loading = false;
      this.alertService.alert('ไม่สามารถเชื่อมต่อระบบได้', 'error');
      console.log(error);
    }
  }

  async sendData(item: any) {
    const confirm = await this.alertService.confirm('ยืนยันการส่ง', 'ต้องการส่งข้อมูล ใช่หรือไม่?')
    if (confirm) {
      try {
        console.log(item);
        const surveilId = item.surveil_id;
        const illAmppart = item.ill_amppart;

        const rs: any = await this.apiService.sendSSJData(surveilId, illAmppart);
        if (rs.ok) {
          this.alertService.alert('ส่งข้อมูลเรียบร้อยแล้ว', 'success');
          this.getHistory();
        } else {
          this.alertService.alert(rs.error || 'เกิดข้อผิดพลาด', 'error');
        }
      } catch (error) {
        this.alertService.alert('เกิดข้อผิดพลาดในการเชื่อมต่อ ', 'error');
      }
    }
  }

}
