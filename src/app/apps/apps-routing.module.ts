import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { PatientComponent } from './patient/patient.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SendComponent } from './send/send.component';
import { AlertService } from '../services/alert.service';
import { AuthGuard } from '../shared/auth.guard';
import { SsjHistoryComponent } from './ssj-history/ssj-history.component';
import { AuthSSJGuard } from '../shared/auth-ssj.guard';


const routes: Routes = [
  {
    path: 'apps',
    canActivate: [AuthGuard],
    component: AppLayoutComponent,
    children: [
      { path: '', redirectTo: 'patient', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'ssj-history',
        component: SsjHistoryComponent,
        canActivate: [AuthSSJGuard]
      },
      { path: 'patient', component: PatientComponent },
      { path: 'send/:svNumber', component: SendComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppsRoutingModule { }
