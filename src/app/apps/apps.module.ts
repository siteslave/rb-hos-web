import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppsRoutingModule } from './apps-routing.module';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { PatientComponent } from './patient/patient.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ApiService } from '../services/api.service';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SendComponent } from './send/send.component';
import { SharedModule } from '../shared.module';
import { AlertService } from '../services/alert.service';
import { AuthGuard } from '../shared/auth.guard';
import { SsjHistoryComponent } from './ssj-history/ssj-history.component';
import { AuthSSJGuard } from '../shared/auth-ssj.guard';
import { NgxLoadingModule } from 'ngx-loading';



@NgModule({
  declarations: [AppLayoutComponent, PatientComponent, DashboardComponent, SendComponent, SsjHistoryComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    AppsRoutingModule,
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  providers: [ApiService, AlertService, AuthGuard, AuthSSJGuard]
})
export class AppsModule { }
