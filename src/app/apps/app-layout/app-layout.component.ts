import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styles: []
})
export class AppLayoutComponent implements OnInit {

  fullname: any;
  hospcode: any;

  constructor() {
    this.fullname = sessionStorage.getItem('fullname');
    this.hospcode = sessionStorage.getItem('hospcode');
  }

  ngOnInit() {
  }

}
