import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import * as _ from 'lodash';

import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styles: [],
})
export class PatientComponent implements OnInit {

  visits: any = [];
  ovstdate: any;

  loading = false;

  constructor(private apiService: ApiService, private router: Router) { }

  async ngOnInit() {
    await this.getVisit();
    await this.getReadySend();

    if (sessionStorage.getItem('ovstDate')) {
      const year = moment(sessionStorage.getItem('ovstDate'), 'YYYY-MM-DD').get('year');
      const month = moment(sessionStorage.getItem('ovstDate'), 'YYYY-MM-DD').get('month');
      const day = moment(sessionStorage.getItem('ovstDate'), 'YYYY-MM-DD').get('date');

      this.ovstdate = {
        day: day,
        month: month + 1,
        year: year
      };

    } else {
      this.ovstdate = new Date();
    }
  }

  async searchVisit() {
    await this.getVisit();
    await this.getReadySend();
  }

  async getVisit() {
    this.loading = true;
    try {
      const _ovstDate = this.ovstdate ? `${this.ovstdate.year}-${this.ovstdate.month}-${this.ovstdate.day}`
        : sessionStorage.getItem('ovstDate') ? sessionStorage.getItem('ovstDate') : moment().format("YYYY-MM-DD");

      sessionStorage.setItem('ovstDate', _ovstDate);

      const rs: any = await this.apiService.getVisit(_ovstDate);

      this.loading = false;

      if (rs.ok) {
        this.visits = rs.rows.map(v => {
          v.begin_date = `${moment(v.begin_date).locale('th').format('DD MMM')} ${moment(v.begin_date).get('year') + 543}`;
          v.report_date = `${moment(v.report_date).locale('th').format('DD MMM')} ${moment(v.report_date).get('year') + 543}`;
          v.is_send = false;
          return v;
        });
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      this.loading = false;
      console.log(error);
    }
  }

  async getReadySend() {
    try {
      const hospcode = sessionStorage.getItem('hospcode');

      const rs: any = await this.apiService.getSSJReadySend(hospcode);
      if (rs.ok) {
        rs.rows.forEach(v => {
          const idx = _.findIndex(this.visits, { sv_number: +v.sv_number });
          if (idx > -1) {
            this.visits[idx].is_send = true;
          }
        });
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  sendData(item: any) {
    const url = `/apps/send/${item.sv_number}`;
    this.router.navigateByUrl(url);
  }

}
