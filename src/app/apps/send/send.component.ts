import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

import * as moment from 'moment';
import * as _ from 'lodash';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styles: []
})
export class SendComponent implements OnInit {

  @ViewChild('content', { static: true }) content;
  @ViewChild('loginContent', { static: true }) loginContent;

  imgSrc = '';
  svNumber: any;
  ptName: any;
  vstDate: any;
  birthdate: any;
  hn: any;
  vn: any;
  sex: any;
  address: any;
  illAddress: any;
  age: any;
  occupation: any;
  cid: any;

  orders: any = [];
  items: any = [];

  itemsSelected = []; // 
  labsForSend = []; // ทั้งหมด

  ovstDate: any;
  currentOrderNumber: any;

  chk1: any;
  chk2: any;
  chk3: any;
  chk4: any;
  chk5: any;
  chk6: any;
  chk7: any;
  chk8: any;
  chk9: any;
  chk10: any;

  chkConfirm: any;

  chwItems = [];
  ampItems = [];
  tmbItems = [];
  mooItems = [];

  chwCode: any;
  ampCode: any;
  tmbCode: any;
  mooCode: any;

  username: any;
  password: any;

  ssjToken: any;

  patientInfo: any = {};
  labs: any = [];
  screens: any = {};
  surveils: any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private modalService: NgbModal,
    private alertService: AlertService) {
    this.svNumber = this.route.snapshot.params.svNumber;
  }

  ngOnInit() {
    this.getInfo();
    this.getChw();
    for (let index = 1; index <= 100; index++) {
      this.mooItems.push(index);
    }
  }

  async getResults(item: any) {
    this.currentOrderNumber = item.lab_order_number;
    this.doGetLabResult(item.lab_order_number);
  }

  async getOrders() {
    try {
      const rs: any = await this.apiService.getLabOrders(this.ovstDate, this.hn);
      if (rs.ok) {
        this.orders = rs.rows.map(v => {
          v.order_date = `${moment(v.order_date).locale('th').format('D MMM')} ${moment(v.order_date).get('year') + 543}`;
          v.order_time = moment(v.order_time, 'HH:mm:ss').format('HH:mm');
          return v;
        });

        this.currentOrderNumber = this.orders[0].lab_order_number;
        this.doGetLabResult(this.orders[0].lab_order_number);
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async getChw() {
    try {
      const rs: any = await this.apiService.getChw();
      this.ampItems = [];
      this.ampCode = null;
      this.tmbItems = [];
      this.tmbCode = null;

      if (rs.ok) {
        this.chwItems = rs.rows;
        this.chwCode = '70';
        this.getAmp();
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async getAmp() {
    try {
      const rs: any = await this.apiService.getAmp(this.chwCode);
      this.tmbItems = [];
      this.tmbCode = null;

      if (rs.ok) {
        this.ampItems = rs.rows;
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async getTmb() {
    try {
      const rs: any = await this.apiService.getTmb(this.chwCode, this.ampCode);
      if (rs.ok) {
        this.tmbItems = rs.rows;
      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async doGetLabResult(orderNumber: any) {
    try {
      const rs: any = await this.apiService.getLabResults(orderNumber);
      if (rs.ok) {
        this.items = [];
        const _items = rs.rows.map(v => {
          const idx = _.findIndex(this.labsForSend, { hos_guid: v.hos_guid });
          if (idx > -1) {
            v.selected = true;
          } else {
            v.selected = false;
          }

          return v;
        });

        this.items = _items;

      } else {
        let _error = rs.error || 'เกิดข้อผิดพลาด';
        this.alertService.alert(_error, 'error');
        console.log(rs.error);
      }
    } catch (error) {
      this.alertService.alert('ไม่สามารถเชื่อมต่อระบบได้', 'error');
      console.log(error);
    }
  }

  async getInfo() {
    try {
      const rs: any = await this.apiService.getInfo(this.svNumber);
      if (rs.ok) {
        const data = rs.rows[0];
        if (data.image) {
          this.imgSrc = `data:image/png;base64,${data.image}`;
        } else {
          this.imgSrc = 'https://randomuser.me/api/portraits/men/78.jpg';
        }

        this.vstDate = `${moment(data.vstdate).format('D MMM')} ${moment(data.vstdate).get('year') + 543}`;
        this.ovstDate = moment(data.vstdate).format('YYYY-MM-DD'); // YYYY-MM-DD

        this.hn = data.hn;
        this.vn = data.vn;
        this.ptName = data.pt_name;
        this.sex = data.sex;
        this.address = data.address;
        this.illAddress = data.ill_address;
        this.age = moment().get('year') - moment(data.birthday).get('year');
        this.occupation = data.occupation_name;
        this.cid = data.cid;
        this.birthdate = moment(data.birthday).format('YYYY-MM-DD');

        this.patientInfo.hospcode = sessionStorage.getItem('hospcode');
        this.patientInfo.cid = data.cid;
        this.patientInfo.hn = data.hn;
        this.patientInfo.pt_name = data.pt_name;
        this.patientInfo.sex = data.sex;
        this.patientInfo.birthday = moment(data.birthday).format('YYYY-MM-DD');
        this.patientInfo.address = data.address;
        this.patientInfo.addrpart = data.addrpart;
        this.patientInfo.moopart = data.moopart;
        this.patientInfo.tmbpart = data.tmbpart;
        this.patientInfo.amppart = data.amppart;
        this.patientInfo.chwpart = data.chwpart;
        this.patientInfo.tel = '';

        this.surveils.hospcode = sessionStorage.getItem('hospcode');
        this.surveils.hn = data.hn;
        this.surveils.vn = data.vn;
        this.surveils.sv_number = this.svNumber;
        this.surveils.pdx = data.pdx;
        this.surveils.vstdate = moment(data.vstdate).format('YYYY-MM-DD');
        this.surveils.report_date = moment(data.report_date).format('YYYY-MM-DD');
        this.surveils.begin_date = moment(data.begin_date).format('YYYY-MM-DD');
        this.surveils.icd_name = data.icd_name;
        this.surveils.code506 = data.code506;
        this.surveils.name506 = data.name506;
        this.surveils.cc = data.cc;
        this.surveils.pe = data.pe;
        this.surveils.ill_addr = data.ill_addr;
        this.surveils.ill_moo = data.ill_moo;
        this.surveils.ill_tmbpart = data.ill_tmbpart;
        this.surveils.ill_amppart = data.ill_amppart;
        this.surveils.ill_chwpart = data.ill_chwpart;

      } else {
        console.log(rs.error);
      }
    } catch (error) {
      console.log(error);
    }
  }

  open() {
    this.modalService
      .open(this.content, {
        ariaLabelledBy: 'modal-basic-title',
        backdrop: 'static',
        size: 'xl'
      })
      .result.then((result) => { }, (reason) => { });

    this.getOrders();
  }

  openConfirm() {
    this.modalService
      .open(this.loginContent, {
        ariaLabelledBy: 'modal-basic-title',
        centered: true,
        backdrop: 'static',
      })
      .result.then((result) => { }, (reason) => { });
  }

  setSelected(chkItem: any, item: any) {
    if (chkItem.checked) {
      this.itemsSelected.push(item);
    } else {
      const idx = _.findIndex(this.itemsSelected, { hos_guid: item.hos_guid });
      if (idx != -1) {
        this.itemsSelected.splice(idx, 1);
      }
    }
  }

  async removeItem(item: any) {
    const confirm = await this.alertService.confirm('ยืนยันการลบ', `ต้องการลบ ${item.lab_items_name_ref} ใช่หรือไม่?`);
    if (confirm) {
      const idx = _.findIndex(this.labsForSend, { hos_guid: item.hos_guid });
      if (idx != -1) {
        this.labsForSend.splice(idx, 1);
      }
    }
  }

  addLabItems() {

    const that = this;

    if (this.itemsSelected) {
      // add items
      this.itemsSelected.forEach(v => {
        const idx = _.findIndex(that.labsForSend, { hos_guid: v.hos_guid });
        if (idx === -1) {
          that.labsForSend.push(v);
        }

      });
    }

    this.modalService.dismissAll();
  }

  async confirmSend() {
    try {
      const rs: any = await this.apiService.doSSJLogin(this.username, this.password);
      if (rs.ok) {
        this.ssjToken = rs.token;
        this.alertService.alert('เข้าสู่ระบบสำเร็จ', 'success');
        console.log(this.patientInfo);

        this.labs = [];

        this.labsForSend.forEach(v => {
          this.labs.push(v);
        });

        if (this.chk1 && this.chk2 && this.chk3 &&
          this.chk4 && this.chk5 && this.chk6 &&
          this.chk7 && this.chk8 && this.chk9 &&
          this.chk10) {

          this.screens.screen_fever = this.chk1;
          this.screens.screen_headache = this.chk2;
          this.screens.screen_eye = this.chk3;
          this.screens.screen_muscle = this.chk4;
          this.screens.screen_ortho = this.chk5;
          this.screens.screen_rash = this.chk6;
          this.screens.screen_skin = this.chk7;
          this.screens.screen_liver = this.chk8;
          this.screens.screen_shock = this.chk9;
          this.screens.screen_tourniquet = this.chk10;
          this.screens.vn = this.vn;
          this.screens.sv_number = this.svNumber;

          this.surveils.diag_dangue = this.chkConfirm;

          // console.log(this.surveils);
          // console.log(this.screens);

          try {
            const rs: any = await this.apiService.saveSurveilSSJ(
              this.ssjToken,
              this.patientInfo,
              this.surveils,
              this.labs,
              this.screens);

            if (rs.ok) {
              this.alertService.alert('ส่งข้อมูลเรียบร้อยแล้ว', 'success');
              this.modalService.dismissAll();
              this.router.navigateByUrl('/apps/patient');
            } else {
              this.alertService.alert(rs.error, 'error');
            }
          } catch (error) {
            console.log(error);
            this.alertService.alert('เกิดข้อผิดพลาด', 'error');
          }

        } else {
          this.alertService.alert('กรุณาตอบแบบอาการและอาการแสดง', 'error');
        }

      } else {
        this.alertService.alert(rs.error, 'error');
      }
    } catch (error) {
      console.log(error);
      this.alertService.alert('เกิดข้อผิดพลาดในการเชื่อมต่อ', 'error')
    }
  }
}
