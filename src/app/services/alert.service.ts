import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  async confirm(title: any, text: any) {
    const confirm = await Swal.fire({
      title: title,
      text: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ใช่!',
      cancelButtonText: 'ไม่ใช่'
    });

    if (confirm.value) {
      return true;
    } else {
      return false;
    }
  }

  alert(message: any, type: any) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
    });

    Toast.fire({
      icon: type,
      title: message
    });

  }
}
