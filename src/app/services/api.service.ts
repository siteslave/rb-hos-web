import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  token: any;
  httpOptions: any;

  constructor(private http: HttpClient,
    @Inject('API_URL') private apiUrl: any,
    @Inject('SSJ_URL') private ssjApi: any) {

  }

  getVisit(ovstDate: any) {
    const token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    const url = `${this.apiUrl}/surveil/list?ovstdate=${ovstDate}`;
    return this.http.get(url, this.httpOptions).toPromise();
  }

  doHISLogin(username: any, password: any) {
    const url = `${this.apiUrl}/login`;
    return this.http.post(url, {
      username: username,
      password: password
    }).toPromise();
  }

  doSSJLogin(username: any, password: any) {
    const url = `${this.ssjApi}/login`;
    return this.http.post(url, {
      username: username,
      password: password
    }).toPromise();
  }

  sendSSJData(surveilId: any, illAmppart: any) {
    const token = sessionStorage.getItem('tokenSSJ');

    const url = `${this.ssjApi}/surveil/send`;
    return this.http.post(url, {
      surveilId: surveilId,
      illAmppart: illAmppart
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    }).toPromise();
  }

  saveSurveilSSJ(token: any,
    patientInfo: any,
    surveil: any,
    labs: any[],
    screen: any
  ) {
    const url = `${this.ssjApi}/surveil`;
    return this.http.post(url, {
      patientInfo: patientInfo,
      surveil: surveil,
      labs: labs,
      screen: screen
    }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    }).toPromise();
  }

  getSSJHistory(query: any, limit: number, offset: number) {
    const token = sessionStorage.getItem('tokenSSJ');

    const url = `${this.ssjApi}/surveil/send-history?query=${query}&limit=${limit}&offset=${offset}`;
    return this.http.get(url, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    }).toPromise();
  }

  getSSJReadySend(hospcode: any) {
    const url = `${this.ssjApi}/ready-send`;
    return this.http.post(url, { hospcode: hospcode }).toPromise();
  }

  getInfo(svNumber: any) {
    const token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    const url = `${this.apiUrl}/surveil/patient-info/${svNumber}`;
    return this.http.get(url, this.httpOptions).toPromise();
  }

  getLabOrders(ovstDate: any, hn: any) {
    const token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    const url = `${this.apiUrl}/surveil/labs?ovstdate=${ovstDate}&hn=${hn}`;
    return this.http.get(url, this.httpOptions).toPromise();
  }

  getLabResults(orderNumber: any) {
    const token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    const url = `${this.apiUrl}/surveil/lab-items?orderNumber=${orderNumber}`;
    return this.http.get(url, this.httpOptions).toPromise();
  }

  getChw() {
    const token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    const url = `${this.apiUrl}/surveil/get-chw`;
    return this.http.get(url, this.httpOptions).toPromise();
  }

  getAmp(chwpart: any) {
    const token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    const url = `${this.apiUrl}/surveil/get-amp?chwpart=${chwpart}`;
    return this.http.get(url, this.httpOptions).toPromise();
  }

  getTmb(chwpart: any, amppart: any) {
    const token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    const url = `${this.apiUrl}/surveil/get-tmb?chwpart=${chwpart}&amppart=${amppart}`;
    return this.http.get(url, this.httpOptions).toPromise();
  }

}
