export const environment = {
  production: true,
  apiUrl: 'http://localhost:3000',
  ssjUrl: 'https://www.bangphaehospital.net/ssj_api'
};
